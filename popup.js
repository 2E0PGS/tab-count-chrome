/*
TabCountChrome

Copyright (C) <2016-2020>  <Peter Stevenson> (2E0PGS)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// Run update once loaded.
ProcessChange();

function GetCurrentWindowTabCount(callback) {
	// Grab list of tabs from current window only.
	chrome.tabs.query({currentWindow:true}, function(tabs) {
		TabCountText = '<center>Current Window Tab Count: ' + tabs.length + '</center>';
		document.getElementById('WindowTabCount').innerHTML = TabCountText;
	});
}

function ProcessChange(tabs) {
	// Process the tab count and update popup.html
	GetCurrentWindowTabCount();
	
}
