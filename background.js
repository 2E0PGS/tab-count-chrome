/*
TabCountChrome

Copyright (C) <2016-2020>  <Peter Stevenson> (2E0PGS)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// Run at program start.
GetAllTabCount();

chrome.tabs.onCreated.addListener(function(tab) {
	GetAllTabCount();
});
  
chrome.tabs.onRemoved.addListener(function(tab) {
	GetAllTabCount();
});

function GetAllTabCount(callback) {
	// Grab complete list of tabs from all windows/instances.
	chrome.tabs.query({}, function(tabs) {
		//chrome.browserAction.setBadgeBackgroundColor({ color: [255, 255, 0, 255] });
		chrome.browserAction.setBadgeText({text: String(tabs.length)});
	});
}