# TabCountChrome

### What is TabCountChrome?

* Simple browser extension for the Chrome Browser.
* Displays the tab count of the current window.
* Displays overall tab count of all instances in a neat counter icon.
* FOSS (Open Source)

### How do I get set up?

* Clone the repo and load the unpacked extension via chrome://extensions/ while using developer mode. 

### Screenshots

* Single Window/Instance ScreenShot:
* ![Screenshot_127.png](https://bitbucket.org/repo/LgK9pd/images/75493230-Screenshot_127.png)
* Multiple Chrome Windows/Instances ScreenShot:
* ![Screenshot_128.png](https://bitbucket.org/repo/LgK9pd/images/479584527-Screenshot_128.png)
* The browser / extension icon with overall tab count:
* ![Screenshot_124.png](https://bitbucket.org/repo/LgK9pd/images/66731451-Screenshot_124.png)
* The interface when clicked with Window Specific tab count:
* ![Screenshot_125.png](https://bitbucket.org/repo/LgK9pd/images/4232954810-Screenshot_125.png)

### Licence

```
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
```